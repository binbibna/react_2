import React, { Component } from 'react'

export default class Nav extends Component {
  state = {
    img_url:'./glassesImage/v1.png',
    text : "GUCCI G8850U",
    decs : "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.",
  };
  doiKinh = (kinh) =>{
    let src = `./glassesImage/${kinh}.png`;
    this.setState({img_url: src});
  }
  doiTen = (ten) =>{
    let name = `${ten}`;
    this.setState({text: name});
  }
  doiKieu = (kieu) =>{
    let detail = `${kieu}`;
    this.setState({decs: detail});
  }


  render() {
    return (
      <div>
        <div className="container item-nav">
            <div className='item' id="id1">
              <div className="item-img">
                <img src={this.state.img_url} alt="" />
              </div>
              <div className="item-txt">
                <h5>{this.state.text}</h5>
                <p>{this.state.decs}</p>
              </div>
            </div>
            <div className='item' id="id2"></div>
        </div>
        <div className="container footer row">
            <div className="item-footer"
            onClick={()=> {
              this.doiKinh('v1');
              this.doiTen(`GUCCI G8850U`);
              this.doiKieu(`Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.`);
            }}
            >
                <img src="./glassesImage/g1.jpg" alt />

            </div>
            <div
            onClick={()=> {
              this.doiKinh('v2');
              this.doiTen(`GUCCI G8759H`);
              this.doiKieu(`Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.`);
            }}
             className="item-footer">
            <img src="./glassesImage/g2.jpg" alt />

            </div>
            <div
            onClick={()=> {
              this.doiKinh('v3');
              this.doiTen(`DIOR D6700HQ`);
              this.doiKieu(`Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.`);
            }}
            className="item-footer">
            <img src="./glassesImage/g3.jpg" alt />

            </div>
            <div
            onClick={()=> {
              this.doiKinh('v4');
              this.doiTen(`DIOR D6005U`);
              this.doiKieu(`Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.`);
            }}
            className="item-footer">
            <img src="./glassesImage/g4.jpg" alt />

            </div>
            <div 
            onClick={()=> {
              this.doiKinh('v5');
              this.doiTen(`PRADA P8750`);
              this.doiKieu(`Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.`);
            }}
            className="item-footer">
            <img src="./glassesImage/g5.jpg" alt />

            </div>
            <div
            onClick={()=> {
              this.doiKinh('v6');
              this.doiTen(`PRADA P9700`);
              this.doiKieu(`Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.`);
            }}
            className="item-footer">
            <img src="./glassesImage/g6.jpg" alt />

            </div>
            <div
            onClick={()=> {
              this.doiKinh('v7');
              this.doiTen(`FENDI F8750`);
              this.doiKieu(`Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.`);
            }}
            className="item-footer">
            <img src="./glassesImage/g7.jpg" alt />

            </div>
            <div
            onClick={()=> {
              this.doiKinh('v8');
              this.doiTen(`FENDI F8500`);
              this.doiKieu(`Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.`);
            }}
            className="item-footer">
            <img src="./glassesImage/g8.jpg" alt />
                
            </div>
            <div
            onClick={()=> {
              this.doiKinh('v9');
              this.doiTen(`FENDI F4300`);
              this.doiKieu(`Light pink square lenses define these sunglasses, ending with amother of pearl effect tip.`);
            }}
            className="item-footer">
            <img src="./glassesImage/g9.jpg" alt />

            </div>
        </div>
      </div>
    )
  }
}
