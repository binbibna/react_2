import React, { Component } from 'react'
import Foote from './Foote'
import Header from './Header'
import "./main.css"
import Nav from './Nav'

export default class Project extends Component {
  render() {
    return (
      <div className="all">
          <Header></Header>
          <Nav>
            
          </Nav>
          <Foote></Foote>
      </div>
    )
  }
}
