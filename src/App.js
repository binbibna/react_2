import logo from './logo.svg';
import './App.css';
import Project from './project/Project';

function App() {
  return (
    <div className="App">
      <Project></Project>
    </div>
  );
}

export default App;
